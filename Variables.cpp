// Programacion 1 LMAD
// Declaracion de variables
//

#include <iostream>
using namespace std;

//Declaración de variables
//se declara el tipo primero (int, char, etc.)
//después el nombre usando "camelCase" (primer letra minúscula, segunda palabra primer letra Mayúscula)
int numeroEntero;
char caracter;

//También se pueden inicializar las variables
//Inicializar: Dar valor a una variable en su declaración.
double numDoble     = 10.010101f;
float flotante      = 3.1415f;

//A toda variable que se encuentra FUERA de una función se le denomina "Variable GLOBAL"

//Main
void main()
{
    //Asignamos un valor a las variables no inicializadas.
    numeroEntero = 5;
    caracter = 'B';
    
    //A toda variable que se encuentra DENTRO de una función se denomina "Variable LOCAL"
    int varLocal = 10;
    
    //cout es una función que imprime en pantalla lo que le indiquemos.
    //Así esta compuesta:
    // cout << contenido;  adicionalmente se pueden ir añadiendo más << para mostrar más contenido
    //Es solo un tipo de dato por "<<" ejemplo: << "Hola" << 10 << "Adios" << 2.1
    //Notese que Hola y Adios son caracteres, y 10 y 2.1 son números, tipos de datos diferentes.
    cout << "Variables declaradas";
    cout << "Entero: " << numeroEntero << endl;
    cout << "Caracter: " << caracter << endl;
    cout << "Doble: " << numDoble << endl;
    cout << "Flotante" << flotante << endl;
    
    //Sirve para esperar un caracter de retorno.
    cin.get();
}

