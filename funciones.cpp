// Programacion 1 LMAD
//Funciones, Procedimientos.

#include <iostream>
using namespace std;

int numero1;
int numero2;
//Declaración de un procedimiento en "cabecera"
void holaMundo()
{
    cout << "Hola Mundo" << endl;
}

//Función Prototipo
int sumaEnteros(int x1, int x2); //<--Importante, lleva punto y coma al final.

int main()
{
    //Mandamos llamar la funcion/procedimiento
    holaMundo();
    
    //Una funcion/procedimiento puede ser llamado N veces en un programa.
    holaMundo();
    holaMundo();
    
    cout << "Dame un numero:";
    cin >> numero1;
    cout << "Dame otro numero:";
    cin >> numero2;
    
    cout << sumaEnteros(numero1, numero2);
    
    return 0;
}

//Como pueden observar se declara en la parte superior e inferior.
//La parte superior es un "prototipo" para decirle al Compilador "hey esta funcion si existe"
//En la parte de abajo del Main se escribe el codigo entre llaves "{ }"
int sumaEnteros(int x1, int x2)
{
    int suma = x1 + x2;
    return suma;
}
//La diferencia entre una funcion y un procedimiento:
//Funcion: Devuelve un resultado.
//Procedimiento: No devuelve nada.


